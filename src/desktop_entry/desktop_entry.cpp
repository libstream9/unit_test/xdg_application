#include <stream9/xdg/desktop_entry.hpp>

#include "../namespace.hpp"

#include <algorithm>
#include <clocale>
#include <iterator>
#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/strings/stream.hpp>
#include <stream9/test/scoped_env.hpp>
#include <stream9/filesystem/tmpfstream.hpp>

namespace testing {

namespace desktop = stream9::xdg;

static boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(desktop_entry_)

    std::string minimal()
    {
        return "[Desktop Entry]\n"
               "Type=Application\n"
               "Name=Foo Viewer\n"
               "Exec=fooview %F\n";
    }

    std::string full()
    {
        return "[Desktop Entry]\n"
               "Version=1.0\n"
               "Type=Application\n"
               "Name=Foo Viewer\n"
               "GenericName = Generic Name\n"
               "Comment=The best viewer for Foo objects available!\n"
               "TryExec=fooview\n"
               "Exec=fooview %F\n"
               "Icon=fooview\n"
               "Path=/tmp\n"
               "MimeType=image/x-foo;\n"
               ;
    }

    desktop::desktop_entry parse_entry(std::string_view const text)
    {
        fs::tmpfstream os;
        os << text << std::flush;

        return { "foo.desktop", os.path().c_str() };
    }

    BOOST_AUTO_TEST_CASE(type_)
    {
        auto const& entry = parse_entry(minimal());

        BOOST_TEST(entry.type() == "Application");
    }

    BOOST_AUTO_TEST_SUITE(name_)

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto text = full();
            text.append(
                "Name[L1_C1.E1@M1]=Foo Viewer (L1_C1.E1@M1)\n"
                "Name[L2_C1]=Foo Viewer (L2_C1)\n"
                "Name[L3@M1]=Foo Viewer (L3@M1)\n"
                "Name[L4]=Foo Viewer (L4)\n"
            );

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.name("L1_C1.E1@M1") == "Foo Viewer (L1_C1.E1@M1)");
            BOOST_TEST(entry.name("L2_C1.E1@M1") == "Foo Viewer (L2_C1)");
            BOOST_TEST(entry.name("L3_C1.E1@M1") == "Foo Viewer (L3@M1)");
            BOOST_TEST(entry.name("L4_C1.E1@M1") == "Foo Viewer (L4)");
            BOOST_TEST(entry.name("L5_C1.E1@M1") == "Foo Viewer");

            BOOST_TEST(entry.name("L2_C1") == "Foo Viewer (L2_C1)");
            BOOST_TEST(entry.name("L4_C1") == "Foo Viewer (L4)");
            BOOST_TEST(entry.name("L5_C1") == "Foo Viewer");

            BOOST_TEST(entry.name("L3@M1") == "Foo Viewer (L3@M1)");
            BOOST_TEST(entry.name("L4@M1") == "Foo Viewer (L4)");
            BOOST_TEST(entry.name("L5@M1") == "Foo Viewer");

            BOOST_TEST(entry.name("L4") == "Foo Viewer (L4)");
            BOOST_TEST(entry.name("L5") == "Foo Viewer");
        }

        BOOST_AUTO_TEST_CASE(default_locale_)
        {
            auto text = full();
            str::ostream os { text };
            os << "Name[" << std::setlocale(LC_MESSAGES, nullptr) << "]"
               << "= default locale\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.name() == "default locale");
        }

        BOOST_AUTO_TEST_CASE(escaped_string_)
        {
            auto text = minimal();
            text.append(
                "Name[L1]=\\s\\n\\t\\r\\\\\\x\\\n"
            );

            auto const& entry = parse_entry(text);
            auto const expected = " \n\t\r\\\\x\\";

            BOOST_TEST(entry.name("L1") == expected);
        }

    BOOST_AUTO_TEST_SUITE_END() // name_

    BOOST_AUTO_TEST_SUITE(version_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.version() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.version() == "1.0");
        }

    BOOST_AUTO_TEST_SUITE_END() // version_

    BOOST_AUTO_TEST_SUITE(generic_name_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.generic_name() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.generic_name() == "Generic Name");
        }

    BOOST_AUTO_TEST_SUITE_END() // generic_name_

    BOOST_AUTO_TEST_SUITE(no_display_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.no_display() == false);
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append(
                "NoDisplay=true\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.no_display() == true);
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append(
                "NoDisplay=false\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.no_display() == false);
        }

    BOOST_AUTO_TEST_SUITE_END() // no_display_

    BOOST_AUTO_TEST_SUITE(comment_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.comment() == "");
        }

        BOOST_AUTO_TEST_CASE(with_locale_)
        {
            auto text = full();
            text.append(
                "Comment[L1_C1.E1@M1]=L1_C1.E1@M1\n"
                "Comment[L2_C1]=L2_C1\n"
                "Comment[L3@M1]=L3@M1\n"
                "Comment[L4]=L4\n"
            );

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.comment("L1_C1.E1@M1") == "L1_C1.E1@M1");
            BOOST_TEST(entry.comment("L2_C1.E1@M1") == "L2_C1");
            BOOST_TEST(entry.comment("L3_C1.E1@M1") == "L3@M1");
            BOOST_TEST(entry.comment("L4_C1.E1@M1") == "L4");
            BOOST_TEST(entry.comment("L5_C1.E1@M1") == "The best viewer for Foo objects available!");

            BOOST_TEST(entry.comment("L2_C1") == "L2_C1");
            BOOST_TEST(entry.comment("L4_C1") == "L4");
            BOOST_TEST(entry.comment("L5_C1") == "The best viewer for Foo objects available!");

            BOOST_TEST(entry.comment("L3@M1") == "L3@M1");
            BOOST_TEST(entry.comment("L4@M1") == "L4");
            BOOST_TEST(entry.comment("L5@M1") == "The best viewer for Foo objects available!");

            BOOST_TEST(entry.comment("L4") == "L4");
            BOOST_TEST(entry.comment("L5") == "The best viewer for Foo objects available!");
        }

        BOOST_AUTO_TEST_CASE(with_default_locale_)
        {
            auto text = full();
            str::ostream os { text };
            os << "Comment[" << std::setlocale(LC_MESSAGES, nullptr) << "]"
               << "= default locale\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.comment() == "default locale");
        }

    BOOST_AUTO_TEST_SUITE_END() // comment_

    BOOST_AUTO_TEST_SUITE(icon_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.icon() == "");
        }

        BOOST_AUTO_TEST_CASE(with_locale_)
        {
            auto text = full();
            text.append(
                "Icon[L1_C1.E1@M1]=L1_C1.E1@M1\n"
                "Icon[L2_C1]=L2_C1\n"
                "Icon[L3@M1]=L3@M1\n"
                "Icon[L4]=L4\n"
            );

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.icon("L1_C1.E1@M1") == "L1_C1.E1@M1");
            BOOST_TEST(entry.icon("L2_C1.E1@M1") == "L2_C1");
            BOOST_TEST(entry.icon("L3_C1.E1@M1") == "L3@M1");
            BOOST_TEST(entry.icon("L4_C1.E1@M1") == "L4");
            BOOST_TEST(entry.icon("L5_C1.E1@M1") == "fooview");

            BOOST_TEST(entry.icon("L2_C1") == "L2_C1");
            BOOST_TEST(entry.icon("L4_C1") == "L4");
            BOOST_TEST(entry.icon("L5_C1") == "fooview");

            BOOST_TEST(entry.icon("L3@M1") == "L3@M1");
            BOOST_TEST(entry.icon("L4@M1") == "L4");
            BOOST_TEST(entry.icon("L5@M1") == "fooview");

            BOOST_TEST(entry.icon("L4") == "L4");
            BOOST_TEST(entry.icon("L5") == "fooview");
        }

        BOOST_AUTO_TEST_CASE(with_default_locale_)
        {
            auto text = full();
            str::ostream os { text };
            os << "Icon[" << std::setlocale(LC_MESSAGES, nullptr) << "]"
               << "= default locale\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.icon() == "default locale");
        }

    BOOST_AUTO_TEST_SUITE_END() // icon_

    BOOST_AUTO_TEST_SUITE(hidden_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.hidden() == false);
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append(
                "Hidden=true\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.hidden() == true);
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append(
                "Hidden=false\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.hidden() == false);
        }

    BOOST_AUTO_TEST_SUITE_END() // hidden_

    BOOST_AUTO_TEST_SUITE(only_show_in_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.only_show_in().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = full();
            text.append(
                "OnlyShowIn="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.only_show_in().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = full();
            text.append(
                "OnlyShowIn=foo"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo" };

            BOOST_TEST(entry.only_show_in() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = full();
            text.append(
                "OnlyShowIn=foo;bar;"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.only_show_in() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(escaped_separator_)
        {
            auto text = full();
            text.append(
                "OnlyShowIn=foo;bar\\;xyzzy"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar\\;xyzzy", "foo" };

            BOOST_TEST(entry.only_show_in() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_values_)
        {
            auto text = full();
            text.append(
                "OnlyShowIn=foo;bar;foo"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.only_show_in() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // only_show_in_

    BOOST_AUTO_TEST_SUITE(not_show_in_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.not_show_in().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = full();
            text.append(
                "NotShowIn="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.not_show_in().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = full();
            text.append(
                "NotShowIn=foo"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo" };

            BOOST_TEST(entry.not_show_in() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = full();
            text.append(
                "NotShowIn=foo;bar;"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.not_show_in() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_values_)
        {
            auto text = full();
            text.append(
                "NotShowIn=foo;bar;foo"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.not_show_in() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // not_show_in_

    BOOST_AUTO_TEST_SUITE(should_show_in_current_desktop_)

        BOOST_AUTO_TEST_CASE(no_only_show_in_entry_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_only_show_in_entry_that_match_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto text = full();
            text.append("OnlyShowIn=bar;foo");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_only_show_in_entry_that_doesnt_match_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto text = full();
            text.append("OnlyShowIn=bar");

            auto const& entry = parse_entry(text);

            BOOST_TEST(!entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_empty_only_show_in_entry_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto text = full();
            text.append("OnlyShowIn=");

            auto const& entry = parse_entry(text);

            BOOST_TEST(!entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_not_show_in_entry_that_doesnt_match_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto text = full();
            text.append("NotShowIn=bar;xyzzy;");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_not_show_in_entry_that_match_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo;bar" };

            auto text = full();
            text.append("NotShowIn=bar;xyzzy;");

            auto const& entry = parse_entry(text);

            BOOST_TEST(!entry.should_show_in_current_desktop());
        }

        BOOST_AUTO_TEST_CASE(have_empty_not_show_in_entry_)
        {
            test::scoped_env env { "XDG_CURRENT_DESKTOP", "foo" };

            auto text = full();
            text.append("NotShowIn=");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.should_show_in_current_desktop());
        }

    BOOST_AUTO_TEST_SUITE_END() // should_show_in_current_desktop_

    BOOST_AUTO_TEST_SUITE(dbus_activatable_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(!entry.dbus_activatable());
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append("DBusActivatable = true");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.dbus_activatable());
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append("DBusActivatable=false");

            auto const& entry = parse_entry(text);

            BOOST_TEST(!entry.dbus_activatable());
        }

    BOOST_AUTO_TEST_SUITE_END() // dbus_activatable_

    BOOST_AUTO_TEST_SUITE(try_exec_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.try_exec() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto text = full();

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.try_exec() == "fooview");
        }

    BOOST_AUTO_TEST_SUITE_END() // try_exec_

    BOOST_AUTO_TEST_SUITE(exec_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const text =
                "[Desktop Entry]\n"
                "Type=Directory\n"
                "Name=Foo\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.exec_raw().empty());
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto const& entry = parse_entry(minimal());

            auto const expected = { "fooview", "%F" };

            BOOST_TEST(entry.exec_raw() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(quoted_1_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=\"foo\"\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo" };

                BOOST_TEST(entry.exec_raw() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(quoted_2_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=\"\\\\\"\\\\$\\\\`\\\\\\\\\"\n";

                auto const& entry = parse_entry(text);

                auto expected = { "\"$`\\" };

                BOOST_TEST(entry.exec_raw() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(without_files_1_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %f xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(without_files_2_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %F xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(without_files_3_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %u xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(without_files_4_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %U xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_a_file_1_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %f xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "bar", "xyzzy" };

                BOOST_TEST(entry.exec("bar") == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_a_file_2_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %u xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "bar", "xyzzy" };

                BOOST_TEST(entry.exec("bar") == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_a_file_3_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %F xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "bar", "xyzzy" };

                BOOST_TEST(entry.exec("bar") == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(with_a_file_4_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %U xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "bar", "xyzzy" };

                BOOST_TEST(entry.exec("bar") == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(other_code_1_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Icon=icon\n"
                    "Exec=foo %i %c xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "icon", "Foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(deprecated_field_code_1_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %d %D %n %N %v %m xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec() == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

        BOOST_AUTO_TEST_CASE(deprecated_field_code_2_)
        {
            try {
                auto text =
                    "[Desktop Entry]\n"
                    "Name=Foo\n"
                    "Type=Application\n"
                    "Exec=foo %d %D %n %N %v %m xyzzy\n";

                auto const& entry = parse_entry(text);

                auto expected = { "foo", "xyzzy" };

                BOOST_TEST(entry.exec("bar") == expected, per_element);
            }
            catch (...) {
                st9::print_error();
                BOOST_TEST(false);
            }
        }

    BOOST_AUTO_TEST_SUITE_END() // exec_

    BOOST_AUTO_TEST_SUITE(path_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.path() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.path() == "/tmp");
        }

    BOOST_AUTO_TEST_SUITE_END() // path_

    BOOST_AUTO_TEST_SUITE(terminal_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(!entry.terminal());
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append("Terminal=true");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.terminal());
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append("Terminal = false");

            auto const& entry = parse_entry(text);

            BOOST_TEST(!entry.terminal());
        }

    BOOST_AUTO_TEST_SUITE_END() // terminal_

    BOOST_AUTO_TEST_SUITE(mime_types_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.mime_types().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = minimal();
            text.append(
                "MimeType="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.mime_types().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = minimal();
            text.append(
                "MimeType=application/foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "application/foo" };

            BOOST_TEST(entry.mime_types() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = minimal();
            text.append(
                "MimeType=application/foo;application/bar\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "application/foo", "application/bar" };

            BOOST_TEST(entry.mime_types() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto text = minimal();
            text.append(
                "MimeType=foo;bar\\;xyzzy\\\\"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo", "bar\\;xyzzy\\" };

            BOOST_TEST(entry.mime_types() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_value_)
        {
            auto text = minimal();
            text.append(
                "MimeType=foo;bar;foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo", "bar" };

            BOOST_TEST(entry.mime_types() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // mime_types_

    BOOST_AUTO_TEST_SUITE(categories_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.categories().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = minimal();
            text.append(
                "Categories="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.categories().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = minimal();
            text.append(
                "Categories=foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo" };

            BOOST_TEST(entry.categories() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = minimal();
            text.append(
                "Categories=foo;bar\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.categories() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto text = minimal();
            text.append(
                "Categories=foo;bar\\;xyzzy\\\\"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar\\;xyzzy\\", "foo" };

            BOOST_TEST(entry.categories() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_value_)
        {
            auto text = minimal();
            text.append(
                "Categories=foo;bar;foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.categories() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // categories_

    BOOST_AUTO_TEST_SUITE(implements_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.implements().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = minimal();
            text.append(
                "Implements="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.implements().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = minimal();
            text.append(
                "Implements=foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo" };

            BOOST_TEST(entry.implements() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = minimal();
            text.append(
                "Implements=foo;bar\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.implements() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto text = minimal();
            text.append(
                "Implements=foo;bar\\;xyzzy\\\\"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar\\;xyzzy\\", "foo" };

            BOOST_TEST(entry.implements() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_value_)
        {
            auto text = minimal();
            text.append(
                "Implements=foo;bar;foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.implements() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // implements_

    BOOST_AUTO_TEST_SUITE(keywords_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.keywords().empty());
        }

        BOOST_AUTO_TEST_CASE(empty_)
        {
            auto text = minimal();
            text.append(
                "Keywords="
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.keywords().empty());
        }

        BOOST_AUTO_TEST_CASE(one_string_)
        {
            auto text = minimal();
            text.append(
                "Keywords=foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "foo" };

            BOOST_TEST(entry.keywords() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(two_string_)
        {
            auto text = minimal();
            text.append(
                "Keywords=foo;bar\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.keywords() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto text = minimal();
            text.append(
                "Keywords=foo;bar\\;xyzzy\\\\"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar\\;xyzzy\\", "foo" };

            BOOST_TEST(entry.keywords() == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(duplicated_value_)
        {
            auto text = minimal();
            text.append(
                "Keywords=foo;bar;foo\n"
            );
            auto const& entry = parse_entry(text);

            auto const expected = { "bar", "foo" };

            BOOST_TEST(entry.keywords() == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // keywords_

    BOOST_AUTO_TEST_SUITE(startup_notify_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(!entry.startup_notify());
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append("StartupNotify=true");

            auto const& entry = parse_entry(text);

            auto const result = entry.startup_notify();

            BOOST_REQUIRE(result);
            BOOST_TEST(*result);
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append("StartupNotify=false");

            auto const& entry = parse_entry(text);

            auto const result = entry.startup_notify();

            BOOST_REQUIRE(result);
            BOOST_TEST(!*result);
        }

    BOOST_AUTO_TEST_SUITE_END() // startup_notify_

    BOOST_AUTO_TEST_SUITE(startup_wm_class_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.startup_wm_class() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto text = minimal();
            text.append("StartupWMClass=foo");

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.startup_wm_class() == "foo");
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto text = minimal();
            text.append("StartupWMClass=foo\\;\\\\"); // \; isn't valid escape sequence

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.startup_wm_class() == "foo\\;\\");
        }

    BOOST_AUTO_TEST_SUITE_END() // startup_wm_class_

    BOOST_AUTO_TEST_SUITE(url_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.url() == "");
        }

        BOOST_AUTO_TEST_CASE(normal_)
        {
            auto const text = "[Desktop Entry]\n"
                "Type=Link\n"
                "Name=Foo\n"
                "URL=foo\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.url() == "foo");
        }

        BOOST_AUTO_TEST_CASE(escaped_value_)
        {
            auto const text = "[Desktop Entry]\n"
                "Type=Link\n"
                "Name=Foo\n"
                "URL=foo\\;\\\\\n";

            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.url() == "foo\\;\\");
        }

    BOOST_AUTO_TEST_SUITE_END() // url_

    BOOST_AUTO_TEST_SUITE(prefer_non_default_gpu_)

        BOOST_AUTO_TEST_CASE(no_entry_)
        {
            auto const& entry = parse_entry(full());

            BOOST_TEST(entry.prefer_non_default_gpu() == false);
        }

        BOOST_AUTO_TEST_CASE(true_)
        {
            auto text = full();
            text.append(
                "PreferNonDefaultGPU=true\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.prefer_non_default_gpu() == true);
        }

        BOOST_AUTO_TEST_CASE(false_)
        {
            auto text = full();
            text.append(
                "PreferNonDefaultGPU=false\n"
            );
            auto const& entry = parse_entry(text);

            BOOST_TEST(entry.prefer_non_default_gpu() == false);
        }

    BOOST_AUTO_TEST_SUITE_END() // prefer_non_default_gpu_

    BOOST_AUTO_TEST_SUITE(application_actions_)

        BOOST_AUTO_TEST_CASE(no_action_)
        {
            auto text = full();
            text.append(
                "[X-Foobar Group]\n"
                "Foo=bar\n"
            );

            auto const& entry = parse_entry(text);

            auto const& groups = entry.application_actions();

            BOOST_TEST(groups.empty());
        }

        BOOST_AUTO_TEST_CASE(one_action_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
            );

            auto const& entry = parse_entry(text);

            auto const& groups = entry.application_actions();

            BOOST_TEST(groups.size() == 1);

            // content of action is tested in other test suite
        }

        BOOST_AUTO_TEST_CASE(two_actions_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;Create;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
                "\n"
                "[Desktop Action Create]\n"
                "Exec=fooview --create-new\n"
                "Name=Create a new Foo!\n"
                "Icon=fooview-new\n"
            );

            auto const& entry = parse_entry(text);

            auto const& groups = entry.application_actions();

            BOOST_TEST(groups.size() == 2);

            // content of action is tested in other test suite
        }

    BOOST_AUTO_TEST_SUITE_END() // application_actions_

    BOOST_AUTO_TEST_SUITE(application_action_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
            );

            auto const& entry = parse_entry(text);

            auto const& group = entry.application_action("Gallery");

            BOOST_CHECK(group);

            // content of action is tested in other test suite
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
            );

            auto const& entry = parse_entry(text);

            auto const& group = entry.application_action("Create");

            BOOST_CHECK(!group);

            // content of action is tested in other test suite
        }

    BOOST_AUTO_TEST_SUITE_END() // application_action_

    BOOST_AUTO_TEST_SUITE(contains_)

        BOOST_AUTO_TEST_CASE(have_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.contains("Name"));
        }

        BOOST_AUTO_TEST_CASE(dont_have_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(!entry.contains("XXX"));
        }

    BOOST_AUTO_TEST_SUITE_END() // contains_

    BOOST_AUTO_TEST_SUITE(value_)

        BOOST_AUTO_TEST_CASE(have_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.value("Name") == "Foo Viewer");
        }

        BOOST_AUTO_TEST_CASE(dont_have_entry_)
        {
            auto const& entry = parse_entry(minimal());

            BOOST_TEST(entry.value("XXX") == "");
        }

    BOOST_AUTO_TEST_SUITE_END() // value_

    BOOST_AUTO_TEST_SUITE(entry_groups_)

        BOOST_AUTO_TEST_CASE(minimal_)
        {
            auto const& entry = parse_entry(minimal());

            auto const& groups = entry.groups();

            auto names = std::ranges::views::transform(
                groups, [](auto&& g) { return g.name(); } );
            auto const expected = { "Desktop Entry" };

            BOOST_TEST(names == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(with_one_action_group_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
            );

            auto const& entry = parse_entry(text);

            auto const& groups = entry.groups();

            auto names = std::ranges::views::transform(
                groups, [](auto&& g) { return g.name(); } );
            auto const expected = { "Desktop Entry", "Desktop Action Gallery" };

            BOOST_TEST(names == expected, per_element);
        }

        BOOST_AUTO_TEST_CASE(with_two_additional_group_)
        {
            auto text = full();
            text.append(
                "Actions=Gallery;\n"
                "\n"
                "[Desktop Action Gallery]\n"
                "Exec=fooview --gallery\n"
                "Name=Browse Gallery\n"
                "\n"
                "[X-Extra Group A]\n"
                "Foo=bar\n"
            );

            auto const& entry = parse_entry(text);

            auto const& groups = entry.groups();

            auto names = std::ranges::views::transform(
                groups, [](auto&& g) { return g.name(); } );
            auto const expected = {
                "Desktop Entry", "Desktop Action Gallery", "X-Extra Group A" };

            BOOST_TEST(names == expected, per_element);
        }

    BOOST_AUTO_TEST_SUITE_END() // entry_groups_

    BOOST_AUTO_TEST_SUITE(entry_group_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto const& entry = parse_entry(minimal());

            auto* const group = entry.group("Desktop Entry");

            BOOST_TEST(group);
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {
            auto const& entry = parse_entry(minimal());

            auto* const group = entry.group("Desktop Action Create");

            BOOST_TEST(!group);
        }

    BOOST_AUTO_TEST_SUITE_END() // entry_group_

BOOST_AUTO_TEST_SUITE_END() // desktop_entry_

} // namespace testing
