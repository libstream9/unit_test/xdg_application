#include "src/parser/mime_info_cache.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/json.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <ranges>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(parser_)
BOOST_AUTO_TEST_SUITE(mime_info_cache_)

    auto parse(std::string_view const text)
    {
        using xdg::parser::mime_info_cache::mime_desktop_file_id_map;
        using stream9::error;

        std::pair<mime_desktop_file_id_map, st9::array<error>> result;
        auto& [cache, errors] = result;

        xdg::parser::mime_info_cache::parse(text, cache, &errors);

        return result;
    }

    BOOST_AUTO_TEST_CASE(real_data_)
    {
        using stream9::path::operator/;

        auto const dir = data_dir() / "real_data" / "data_dir" / "applications";
        auto text = fs::load_string(dir / "mimeinfo.cache");
        BOOST_TEST(!text.empty());

        auto const& [entries, errors] = parse(text);

        BOOST_TEST(entries.size() == 338);
        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_data_)
    {
        auto const& [entries, errors] = parse("");

        BOOST_TEST(entries.size() == 0);
        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(zero_entry_)
    {
        std::string text = "[MIME Cache]\n";

        auto const& [entries, errors] = parse(text);

        BOOST_TEST(entries.size() == 0);
        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(one_entry_)
    {
        std::string text = "[MIME Cache]\n"
            "foo/bar=foo.desktop;bar.desktop;\n";

        auto const& [entries, errors] = parse(text);

        auto jv = json::value_from(entries);

        json::object expected {
            { "foo/bar", "foo.desktop;bar.desktop;" },
        };

        BOOST_TEST(jv == expected);

        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(two_entry_)
    {
        std::string text = "[MIME Cache]\n"
            "foo/bar=foo.desktop;bar.desktop;\n"
            "xxx/yyy=xxx.desktop\n"
            ;

        auto const& [entries, errors] = parse(text);

        auto jv = json::value_from(entries);

        json::object expected {
            { "foo/bar", "foo.desktop;bar.desktop;" },
            { "xxx/yyy", "xxx.desktop" },
        };

        BOOST_TEST(jv == expected);
        BOOST_TEST(errors.empty());
    }

    BOOST_AUTO_TEST_CASE(duplicated_mime_type_)
    {
        std::string text = "[MIME Cache]\n"
            "foo/bar=foo.desktop;bar.desktop;\n"
            "xxx/yyy=xxx.desktop\n"
            "xxx/yyy=xxx.desktop\n"
            ;

        auto const& [entries, errors] = parse(text);

        auto jv = json::value_from(entries);

        json::object expected {
            { "foo/bar", "foo.desktop;bar.desktop;" },
            { "xxx/yyy", "xxx.desktop" },
        };

        using xdg::parser::mime_info_cache::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::duplicated_key);
    }

    BOOST_AUTO_TEST_CASE(unknown_group_1_)
    {
        std::string text = "[MIME Cache X]\n"
            "foo/bar=foo.desktop;bar.desktop;\n"
            ;

        auto const& [entries, errors] = parse(text);

        BOOST_TEST(entries.size() == 0);

        using xdg::parser::mime_info_cache::errc;
        using stream9::error;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::invalid_group_name);
    }

    BOOST_AUTO_TEST_CASE(unknown_group_2_)
    {
        std::string text = "[MIME Cache]\n"
            "foo/bar=foo.desktop;bar.desktop;\n"
            "\n"
            "[Blah]\n"
            "foo=bar\n"
            ;

        auto const& [entries, errors] = parse(text);

        auto jv = json::value_from(entries);

        json::object expected {
            { "foo/bar", "foo.desktop;bar.desktop;" },
        };

        using xdg::parser::mime_info_cache::errc;

        BOOST_REQUIRE(errors.size() == 1);
        BOOST_TEST(errors[0].why() == errc::invalid_group_name);
    }

BOOST_AUTO_TEST_SUITE_END() // mime_info_cache_
BOOST_AUTO_TEST_SUITE_END() // parser_

} // namespace testing
