#include "src/parser/exec_string.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

namespace parser = stream9::xdg::parser;

boost::test_tools::per_element per_element;

BOOST_AUTO_TEST_SUITE(parser_)
BOOST_AUTO_TEST_SUITE(exec_string_)

    BOOST_AUTO_TEST_CASE(empty_1_)
    {
        auto const& args = parser::exec_string::parse("");

        BOOST_TEST(args.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_2_)
    {
        auto const& args = parser::exec_string::parse("   ");

        BOOST_TEST(args.empty());
    }

    BOOST_AUTO_TEST_CASE(one_arg_)
    {
        auto const& args = parser::exec_string::parse("foo");

        auto const expected = { "foo", };

        BOOST_TEST(args == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(two_args_1_)
    {
        auto const& args = parser::exec_string::parse("/usr/lib/firefox/firefox %u");

        auto const expected = { "/usr/lib/firefox/firefox", "%u" };

        BOOST_TEST(args == expected, per_element);
    }

    BOOST_AUTO_TEST_CASE(two_args_2_)
    {
        auto const& args = parser::exec_string::parse("foo \"bar %f\"");

        auto const expected = { "foo", "bar %f" };

        BOOST_TEST(args == expected, per_element);
    }

BOOST_AUTO_TEST_SUITE_END() // exec_string_
BOOST_AUTO_TEST_SUITE_END() // parser_

} // namespace testing
