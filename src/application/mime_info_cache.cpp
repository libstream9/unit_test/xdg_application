#include "src/application/mime_info_cache.hpp"

#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/tmpfstream.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace testing {

BOOST_AUTO_TEST_SUITE(mime_info_cache_)

    BOOST_AUTO_TEST_SUITE(find_)

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            fs::tmpfstream os;

            os << "[MIME Cache]\n"
               << "foo/bar=foo.desktop;bar.desktop;\n"
               << std::flush;

            apps::mime_info_cache cache { os.path().c_str() };

            auto ids = cache.find("xxx/yyy");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_empty_)
        {
            fs::tmpfstream os;

            os << "[MIME Cache]\n"
               << "foo/bar=\n"
               << std::flush;

            apps::mime_info_cache cache { os.path().c_str() };

            auto ids = cache.find("foo/bar");

            BOOST_TEST(ids.empty());
        }

        BOOST_AUTO_TEST_CASE(found_one_id_)
        {
            fs::tmpfstream os;

            os << "[MIME Cache]\n"
               << "foo/bar=foo.desktop;\n"
               << std::flush;

            apps::mime_info_cache cache { os.path() };

            auto ids = cache.find("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(found_two_ids_)
        {
            fs::tmpfstream os;

            os << "[MIME Cache]\n"
               << "foo/bar=foo.desktop;bar.desktop\n"
               << std::flush;

            apps::mime_info_cache cache { os.path().c_str() };

            auto ids = cache.find("foo/bar");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = { "foo.desktop", "bar.desktop" };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(real_data_)
        {
            using stream9::path::operator/;

            auto const dir =
                data_dir() / "real_data" / "data_dir" / "applications";
            apps::mime_info_cache cache { dir / "mimeinfo.cache" };

            auto ids = cache.find("application/pdf");

            std::vector<std::string_view> result; // BOOST_TEST require const_iterator :(
            std::ranges::copy(ids, std::back_inserter(result));

            auto const expected = {
                "chromium.desktop",
                "firefox.desktop",
                "okularApplication_pdf.desktop",
                "org.kde.mobile.okular_pdf.desktop",
            };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // find_

BOOST_AUTO_TEST_SUITE_END() // mime_info_cache_

} // namespace testing
