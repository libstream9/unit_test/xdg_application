#include <stream9/xdg/application/v1/database_updater.hpp>

#include "../make_path.hpp"

#include "create_directory_tree.hpp"
#include "data_dir.hpp"
#include "namespace.hpp"

#include <stream9/xdg/application/v1/database.hpp>

#include <stream9/array.hpp>
#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/projection.hpp>
#include <stream9/ranges/find.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/strings/view/join.hpp>
#include <stream9/test/scoped_env.hpp>
#include <stream9/unique_table.hpp>

#include <boost/test/unit_test.hpp>

#include <thread>

namespace testing {

namespace app1 = st9::xdg::applications::v1;

namespace lx { using namespace stream9::linux; }

using st9::string;
using st9::string_view;
using st9::array;
using st9::unique_table;
using st9::opt;

class desktop_entry
{
public:
    desktop_entry(string_view name, json::object content)
        : m_name { name }
        , m_content { std::move(content) }
    {}

    auto begin() const { return m_content.begin(); }
    auto end() const { return m_content.end(); }

    string_view name() const noexcept { return m_name; }
    string filename() const { return m_name + ".desktop"; }

    opt<string_view> mime_type() const noexcept
    {
        auto o_s = json::find_string(m_content, "MimeType");
        if (o_s) {
            return *o_s;
        }
        else {
            return stream9::null;
        }
    }

private:
    string m_name;
    json::object m_content;
};

std::ostream&
operator<<(std::ostream& os, desktop_entry const& e)
{
    os << "[Desktop Entry]" << std::endl;
    for (auto const& [key, value]: e) {
        os << key << "=";
        if (value.is_array()) {
            for (auto const& v: value.get_array()) {
                if (v.is_string()) {
                    os << v.get_string().c_str() << ";";
                }
            }
        }
        else {
            assert(value.is_string());

            os << value.get_string().c_str();
        }
        os << std::endl;
    }
    return os;
}

class data_directory
{
public:
    data_directory(string_view path)
        : m_path { path }
    {
        fs::mkdir_recursive(m_path / "applications");
    }

    // accessor
    string_view path() const noexcept { return m_path; }

    auto const& applications() const noexcept { return m_entries; }
    auto const& added_associations() const noexcept { return m_added_assoc; }

    // modifier
    void add_desktop_entry(desktop_entry e)
    {
        st9::ofstream ofs { m_path / "applications" / e.filename(), ofs.trunc };
        ofs << e;

        m_entries.insert(std::move(e));
    }

    void remove_desktop_entry(string_view name)
    {
        auto it = std::ranges::find(m_entries, name, st9::project<&desktop_entry::name>());
        if (it != m_entries.end()) {
            auto p = m_path / "applications" / it->filename();
            lx::unlink(p);

            m_entries.erase(it);
        }
    }

    void add_association(string_view mime_type, string_view name)
    {
        m_added_assoc.emplace(
            string(mime_type), string(name) + ".desktop" );
    }

    void remove_association(string_view mime_type, string_view name)
    {
        m_removed_assoc.emplace(
            string(mime_type), string(name) + ".desktop" );
    }

    void add_default_application(string_view mime_type, string_view name)
    {
        m_default_apps.emplace(
            string(mime_type), string(name) + ".desktop" );
    }

    void update_cache();

private:
    string m_path;
    array<desktop_entry> m_entries;
    unique_table<string, string> m_added_assoc;
    unique_table<string, string> m_removed_assoc;
    unique_table<string, string> m_default_apps;
};

void data_directory::
update_cache()
{
    {
        st9::ofstream ofs { m_path / "applications/mimeinfo.cache", ofs.trunc };
        ofs << "[MIME Cache]" << std::endl;
        for (auto const& ent: m_entries) {
            ofs << ent.mime_type() << "=" << ent.filename() << std::endl;
        }
    }
    {
        st9::ofstream ofs { m_path / "mimeapps.list", ofs.trunc };
        if (m_added_assoc.size() != 0) {
            ofs << "[Added Associations]" << std::endl;
            for (auto const& assoc: m_added_assoc) {
                ofs << assoc.key << "=" << assoc.value << std::endl;
            }
        }
        if (m_removed_assoc.size() != 0) {
            ofs << "[Removed Associations]" << std::endl;
            for (auto const& assoc: m_removed_assoc) {
                ofs << assoc.key << "=" << assoc.value << std::endl;
            }
        }
        if (m_default_apps.size() != 0) {
            ofs << "[Default Applications]" << std::endl;
            for (auto const& assoc: m_default_apps) {
                ofs << assoc.key << "=" << assoc.value << std::endl;
            }
        }
    }
}

BOOST_AUTO_TEST_SUITE(database_updater_v1_)

    struct event_recorder : public app1::database_updater::event_handler
    {
        json::array events;

        void desktop_entry_added(string_view id, string_view path) noexcept override
        {
            events.push_back(json::object {
                { "type", "desktop_entry_added" },
                { "id", id },
                { "path", path }
            });
        }

        void desktop_entry_changed(string_view id, string_view path) noexcept override
        {
            events.push_back(json::object {
                { "type", "desktop_entry_changed" },
                { "id", id },
                { "path", path }
            });
        }

        void desktop_entry_deleted(string_view id) noexcept override
        {
            events.push_back(json::object {
                { "type", "desktop_entry_deleted" },
                { "id", id },
            });
        }
    };

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        fs::temporary_directory tmp;

        data_directory data_home { tmp.path() / "data_home" };
        data_home.update_cache();

        test::scoped_env env1 { "XDG_CONFIG_HOME", data_home.path() };
        test::scoped_env env2 { "XDG_DATA_HOME", data_home.path() };
        test::scoped_env env3 { "XDG_CONFIG_DIRS", data_home.path() };
        test::scoped_env env4 { "XDG_DATA_DIRS", data_home.path() };

        app1::database db;
        app1::database_updater upd { db };
    }

    BOOST_AUTO_TEST_CASE(add_apps_1_)
    {
        fs::temporary_directory tmp;

        data_directory data_home { tmp.path() / "data_home" };
        data_home.update_cache();

        test::scoped_env env1 { "XDG_CONFIG_HOME", data_home.path() };
        test::scoped_env env2 { "XDG_DATA_HOME", data_home.path() };
        test::scoped_env env3 { "XDG_CONFIG_DIRS", data_home.path() };
        test::scoped_env env4 { "XDG_DATA_DIRS", data_home.path() };

        app1::database db;
        app1::database_updater upd { db };

        event_recorder h;
        upd.add_event_handler(h);

        auto o_app1 = db.find_default_application("foo/bar");
        BOOST_TEST(!o_app1);

        using namespace std::literals;
        std::this_thread::sleep_for(100ms);

        data_home.add_desktop_entry({
            "foo", json::object {
                { "Type", "Application" },
                { "Name", "foo" },
                { "MimeType", "foo/bar" },
                { "Exec", "foo" }
            }
        });
        data_home.update_cache();

        upd.process_events();

        json::array expected {
            json::object {
                { "type", "desktop_entry_added" },
                { "id", "foo.desktop" },
                { "path", data_home.path() / "foo.desktop" },
            }
        };

        BOOST_TEST(h.events == expected);

        auto o_app2 = db.find_default_application("foo/bar");
        BOOST_REQUIRE(o_app2);
        BOOST_TEST(o_app2->name() == "foo");
    }

    BOOST_AUTO_TEST_CASE(remove_apps_1_)
    {
        fs::temporary_directory tmp;

        data_directory data_home { tmp.path() / "data_home" };

        data_home.add_desktop_entry({
            "foo", json::object {
                { "Type", "Application" },
                { "Name", "foo" },
                { "MimeType", "foo/bar" },
                { "Exec", "foo" }
            }
        });
        data_home.update_cache();

        test::scoped_env env1 { "XDG_CONFIG_HOME", data_home.path() };
        test::scoped_env env2 { "XDG_DATA_HOME", data_home.path() };
        test::scoped_env env3 { "XDG_CONFIG_DIRS", data_home.path() };
        test::scoped_env env4 { "XDG_DATA_DIRS", data_home.path() };

        app1::database db;
        app1::database_updater upd { db };

        event_recorder h;
        upd.add_event_handler(h);

        auto o_app1 = db.find_default_application("foo/bar");
        BOOST_REQUIRE(o_app1);
        BOOST_TEST(o_app1->name() == "foo");

        using namespace std::literals;
        std::this_thread::sleep_for(100ms);

        data_home.remove_desktop_entry("foo");
        data_home.update_cache();

        upd.process_events();

        json::array expected {
            json::object {
                { "type", "desktop_entry_deleted" },
                { "id", "foo.desktop" },
            }
        };

        BOOST_TEST(h.events == expected);

        auto o_app2 = db.find_default_application("foo/bar");
        BOOST_REQUIRE(!o_app2);
    }

    BOOST_AUTO_TEST_CASE(update_mimeapps_list_1_)
    {
        fs::temporary_directory tmp;

        data_directory data_home { tmp.path() / "data_home" };

        data_home.add_desktop_entry({
            "foo", json::object {
                { "Type", "Application" },
                { "Name", "foo" },
                { "MimeType", "foo/bar" },
                { "Exec", "foo" }
            }
        });
        data_home.update_cache();

        test::scoped_env env1 { "XDG_CONFIG_HOME", data_home.path() };
        test::scoped_env env2 { "XDG_DATA_HOME", data_home.path() };
        test::scoped_env env3 { "XDG_CONFIG_DIRS", data_home.path() };
        test::scoped_env env4 { "XDG_DATA_DIRS", data_home.path() };

        app1::database db;
        app1::database_updater upd { db };

        auto o_app1 = db.find_default_application("bar/foo");
        BOOST_TEST(!o_app1);

        using namespace std::literals;
        std::this_thread::sleep_for(100ms);

        data_home.add_association("bar/foo", "foo");
        data_home.update_cache();

        upd.process_events();

        auto o_app2 = db.find_default_application("bar/foo");
        BOOST_REQUIRE(o_app2);
        BOOST_TEST(o_app2->name() == "foo");
    }

BOOST_AUTO_TEST_SUITE_END() // database_updater_v1_

} // namespace testing
