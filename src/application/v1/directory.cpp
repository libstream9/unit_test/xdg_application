#include "src/application/v1/directory.hpp"

#include "namespace.hpp"

#include <ranges>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/mkdir_recursive.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/test/scoped_env.hpp>

namespace testing {

namespace path = stream9::path;

using path::operator/;
using stream9::string;
using stream9::string_view;

BOOST_AUTO_TEST_SUITE(directory_)

    static void make_mime_info_cache(string_view path)
    {
        fs::mkdir_recursive(path::dirname(path));
        st9::ofstream os { path };

        os << "[MIME Cache]\n"
           << "foo/bar=foo.desktop\n";
    }

    static void make_mime_apps_list(string_view path)
    {
        st9::ofstream os { path };

        os << "[Added Associations]\n"
           << "foo/bar=added.desktop;bar.desktop\n"
           << "\n"
           << "[Removed Associations]\n"
           << "foo/bar=removed.desktop;foo.desktop\n"
           << "\n"
           << "[Default Applications]\n"
           << "foo/bar=default.desktop\n";
    }

    static void make_desktop_mime_apps_list(string_view path)
    {
        st9::ofstream os { path };

        os << "[Default Applications]\n"
           << "foo/bar=desktop-default.desktop\n";
    }

    BOOST_AUTO_TEST_CASE(installed_applications_)
    {
        try {
            fs::temporary_directory dir;
            test::scoped_env e { "XDG_CURRENT_DESKTOP", "LXQt;Foo;" };

            make_mime_info_cache(dir.path() / "applications/mimeinfo.cache");
            make_mime_apps_list(dir.path() / "mimeapps.list");
            make_desktop_mime_apps_list(dir.path() / "lxqt-mimeapps.list");

            apps::directory d { dir.path() };

            auto ids = d.installed_applications("foo/bar");
            auto expected = { "foo.desktop" };

            std::vector<std::string_view> result;
            std::ranges::copy(ids, std::back_inserter(result));

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }
        catch (...) {
            stream9::print_error();
        }
    }

    BOOST_AUTO_TEST_CASE(all_applications_)
    {
        fs::temporary_directory dir;
        auto mime_info_path = dir.path() / "applications/mimeinfo.cache";

        fs::mkdir_recursive(path::dirname(mime_info_path));
        st9::ofstream os;

        os.open(dir.path() / "applications" / "a1.desktop");
        os << "[Desktop Entry]\n"
           << "Name=a1\n"
           << "Type=Application\n"
           << "Exec=ls\n";
        os.close();

        os.open(dir.path() / "applications" / "a2.desktop");
        os << "[Desktop Entry]\n"
           << "Name=a2\n"
           << "Type=Directory\n";
        os.close();

        os.open(dir.path() / "applications" / "a3.desktop");
        os << "[Desktop Entry]\n"
           << "Name=a3\n"
           << "Type=Application\n"
           << "Exec=ls\n";
        os.close();

        apps::directory d { dir.path() };

        std::vector<string> result;
        d.all_applications([&](auto&& e) { result.push_back(e.id()); });

        auto expected = { "a1", "a3" };

        BOOST_TEST(result == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(added_associations_)
    {
        fs::temporary_directory dir;
        test::scoped_env e { "XDG_CURRENT_DESKTOP", "LXQt;Foo;" };

        make_mime_info_cache(dir.path() / "applications/mimeinfo.cache");
        make_mime_apps_list(dir.path() / "mimeapps.list");
        make_desktop_mime_apps_list(dir.path() / "lxqt-mimeapps.list");

        apps::directory d { dir.path() };

        auto ids = d.added_associations("foo/bar");
        auto expected = { "added.desktop", "bar.desktop" };

        std::vector<std::string_view> result;
        std::ranges::copy(ids, std::back_inserter(result));

        BOOST_TEST(result == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(removed_associations_)
    {
        fs::temporary_directory dir;
        test::scoped_env e { "XDG_CURRENT_DESKTOP", "LXQt;Foo;" };

        make_mime_info_cache(dir.path() / "applications/mimeinfo.cache");
        make_mime_apps_list(dir.path() / "mimeapps.list");
        make_desktop_mime_apps_list(dir.path() / "lxqt-mimeapps.list");

        apps::directory d { dir.path() };

        auto ids = d.removed_associations("foo/bar");
        auto expected = { "removed.desktop", "foo.desktop" };

        std::vector<std::string_view> result;
        std::ranges::copy(ids, std::back_inserter(result));

        BOOST_TEST(result == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(default_applications_)
    {
        fs::temporary_directory dir;
        test::scoped_env e { "XDG_CURRENT_DESKTOP", "LXQt;Foo;" };

        make_mime_info_cache(dir.path() / "applications/mimeinfo.cache");
        make_mime_apps_list(dir.path() / "mimeapps.list");
        make_desktop_mime_apps_list(dir.path() / "lxqt-mimeapps.list");

        apps::directory d { dir.path() };

        auto ids = d.default_applications("foo/bar");
        auto expected = { "desktop-default.desktop", "default.desktop" };

        std::vector<std::string_view> result;
        std::ranges::copy(ids, std::back_inserter(result));

        BOOST_TEST(result == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(duplicated_desktop_name_)
    {
        using stream9::path::basename;

        fs::temporary_directory dir;
        test::scoped_env e { "XDG_CURRENT_DESKTOP", "LXQt;Foo;lxqt" };

        make_mime_info_cache(dir.path() / "applications/mimeinfo.cache");
        make_mime_apps_list(dir.path() / "mimeapps.list");
        make_desktop_mime_apps_list(dir.path() / "lxqt-mimeapps.list");

        apps::directory d { dir.path() };

        auto filenames = std::ranges::views::transform(d.desktop_mime_apps_lists(),
            [](auto&& l) { return basename(l.path()); }
        );

        auto expected = { "lxqt-mimeapps.list" };

        BOOST_TEST(filenames == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // directory_

} // namespace testing
