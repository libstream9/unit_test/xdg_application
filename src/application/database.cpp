#include <stream9/xdg/application/database.hpp>

#include "make_path.hpp"

#include "../create_directory_tree.hpp"
#include "../data_dir.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/mime/database.hpp>

#include <stream9/cpu_timer.hpp>
#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/json.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/test/ostream_captureer.hpp>
#include <stream9/test/scoped_env.hpp>
#include <stream9/to_string.hpp>
#include <stream9/unique_array.hpp>

#include <algorithm>
#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

using st9::string;
using path::operator/;

namespace but = boost::unit_test;

BOOST_AUTO_TEST_SUITE(database_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        using T = apps::database;

        static_assert(std::move_constructible<T>);
        static_assert(std::destructible<T>);
    }

    BOOST_AUTO_TEST_SUITE(find_applications_)

        BOOST_AUTO_TEST_CASE(real_data_1_)
        {
            auto const& data_path = data_dir() / "real_data/data_dir";
            auto dummy = data_dir() / "dummy";
            test::scoped_env e1 { "XDG_CONFIG_HOME", dummy.c_str() };
            test::scoped_env e2 { "XDG_CONFIG_DIRS", dummy.c_str() };
            test::scoped_env e3 { "XDG_DATA_HOME", dummy.c_str() };
            test::scoped_env e4 { "XDG_DATA_DIRS", data_path.c_str() };

            xdg::mime::database mime_db;
            apps::database db { mime_db };
            test::ostream_captureer os { std::cerr };

            auto apps = db.find_applications("application/pdf");

            json::array result;
            for (auto const& app: apps) {
                result.push_back(app.name());
            }

            json::array expected {
                "Chromium", "Firefox", "Okular", "Reader",
            };

            BOOST_TEST(result == expected);
        }

        BOOST_AUTO_TEST_CASE(real_data_2_)
        {
            auto const& data_path = data_dir() / "real_data/data_dir";
            auto dummy = data_dir() / "dummy";
            test::scoped_env e1 { "XDG_CONFIG_HOME", dummy.c_str() };
            test::scoped_env e2 { "XDG_CONFIG_DIRS", dummy.c_str() };
            test::scoped_env e3 { "XDG_DATA_HOME", dummy.c_str() };
            test::scoped_env e4 { "XDG_DATA_DIRS", data_path.c_str() };

            xdg::mime::database mime_db;
            apps::database db { mime_db };
            test::ostream_captureer os { std::cerr };

            // application/json has application/javascript, application/ecmascript,
            // application/x-executable, text/plain as its ancestors
            auto apps = db.find_applications("application/json");

            json::array result;
            for (auto const& app: apps) {
                result.push_back(app.name());
            }

            json::array expected {
                "Firefox", // assoc. with application/javascript
                "Emacs",   // all of this and below are assoc. with text/plain
                "GVim",
                "Okular",
                "Kate",
                "Reader",
                "Vim",
            };

            BOOST_TEST(result == expected);
        }

        BOOST_AUTO_TEST_CASE(basic_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": {}, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": { )"
                R"(   "applications": { )"
                R"(     "foo-home.desktop": [ "[Desktop Entry]", )"
                R"(                           "Type=Application", )"
                R"(                           "Name=FooHome", )"
                R"(                           "Exec=foo" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo-home.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "bar.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Bar", )"
                R"(                      "Exec=bar" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop", )"
                R"(                         "bar/xyzzy=bar.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": { )"
                R"(   "applications": { )"
                R"(     "foo2.desktop": [ "[Desktop Entry]", )"
                R"(                       "Type=Application", )"
                R"(                       "Name=Foo2", )"
                R"(                       "Exec=foo2" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo2.desktop" ] )"
                R"(   } )"
                R"( } )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto apps = db.find_applications("foo/bar");

            auto expected = { "FooHome", "Foo", "Foo2" };

            std::vector<std::string> result;
            for (auto const& app: apps) {
                result.push_back(app.name());
            }

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(add_associations_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": {}, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=FooHome", )"
                R"(                      "Exec=foo" ], )"
                R"(     "bar.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=BarHome", )"
                R"(                      "Exec=bar" ], )"
                R"(     "mimeapps.list": [ "[Added Associations]", )"
                R"(                        "foo/bar=bar.desktop" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop", )"
                R"(                         "bar/xyzzy=bar.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "bar.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Bar", )"
                R"(                      "Exec=bar" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop", )"
                R"(                         "bar/xyzzy=bar.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo2", )"
                R"(                      "Name=foo2" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( } )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto apps = db.find_applications("foo/bar");

            auto const expected = { "BarHome", "FooHome" };

            std::vector<std::string> result;
            for (auto const& app: apps) {
                result.push_back(app.name());
            }

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

        BOOST_AUTO_TEST_CASE(removed_associations_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": {}, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=FooHome", )"
                R"(                      "Exec=foo" ], )"
                R"(     "bar.desktop": [ "[Desktop Entry]", )" // removed association doesn't affect this one
                R"(                      "Type=Application", )"
                R"(                      "Name=BarHome", )"
                R"(                      "Exec=bar" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop;bar.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "bar.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Bar", )"
                R"(                      "Exec=bar" ], )"
                R"(     "mimeapps.list": [ "[Removed Associations]", )"
                R"(                        "foo/bar=bar.desktop;xyzzy.desktop" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop", )"
                R"(                         "bar/xyzzy=bar.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": { )"
                R"(   "applications": { )"
                R"(     "xyzzy.desktop": [ "[Desktop Entry]", )" // this one should be removed
                R"(                      "Type=Application", )"
                R"(                      "Name=Xyzzy", )"
                R"(                      "Exec=xyzzy" ],)"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=xyzzy.desktop" ] )"
                R"(   } )"
                R"( } )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto apps = db.find_applications("foo/bar");

            auto const expected = { "FooHome", "BarHome" };

            std::vector<std::string> result;
            for (auto const& app: apps) {
                result.push_back(app.name());
            }

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // find_applications_

    BOOST_AUTO_TEST_SUITE(find_default_application_)

        BOOST_AUTO_TEST_CASE(real_data_, *but::enabled())
        {
            auto const& data_path = data_dir() / "real_data/data_dir";
            test::scoped_env e1 { "XDG_DATA_DIRS", data_path };
            test::scoped_env e2 { "XDG_CONFIG_DIRS", data_dir() / "dummy" };
            test::scoped_env e3 { "XDG_CONFIG_HOME", data_dir() / "real_data/config_home" };
            test::scoped_env e4 { "XDG_DATA_HOME", data_dir() / "dummy" };
            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto o_app = db.find_default_application("application/pdf");

            BOOST_TEST_REQUIRE(!!o_app);

            BOOST_TEST(o_app->name() == "Chromium");
        }

        BOOST_AUTO_TEST_CASE(basic_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": { )"
                R"(   "mimeapps.list": [ "[Default Applications]", )"
                R"(                      "foo/bar=xxx.desktop;foo.desktop" ] )"
                R"( }, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": {}, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": {} )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto o_app = db.find_default_application("foo/bar");
            BOOST_TEST_REQUIRE(!!o_app);
            BOOST_TEST(o_app->name() == "Foo");
        }

        BOOST_AUTO_TEST_CASE(directory_position_doesnt_matter_for_default_application_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": {}, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=FooHome", )"
                R"(                      "Exec=foo" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "mimeapps.list": [ "[Default Applications]", )"
                R"(                        "foo/bar=xxx.desktop;foo.desktop" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": {} )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto o_app = db.find_default_application("foo/bar");
            BOOST_TEST_REQUIRE(!!o_app);
            BOOST_TEST(o_app->name() == "FooHome");
        }

        BOOST_AUTO_TEST_CASE(directory_at_which_association_has_added_doesnt_matter_neither_)
        {
            fs::temporary_directory tmp;

            auto const tree = "{"
                R"( "config_home": { )"
                R"(   "mimeapps.list": [ "[Default Applications]", )"
                R"(                      "xxx/yyy=xxx.desktop;foo.desktop" ] )"
                R"( }, )"
                R"( "config_dir1": {}, )"
                R"( "config_dir2": {}, )"
                R"( "data_home": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )" // this should be picked up
                R"(                      "Type=Application", )"
                R"(                      "Name=FooHome", )"
                R"(                      "Exec=foo" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir1": { )"
                R"(   "applications": { )"
                R"(     "foo.desktop": [ "[Desktop Entry]", )"
                R"(                      "Type=Application", )"
                R"(                      "Name=Foo", )"
                R"(                      "Exec=foo" ], )"
                R"(     "mimeapps.list": [ "[Added Associations]", )" // even though it is associated with mime type at here, lower preferenced directory
                R"(                        "xxx/yyy=foo.desktop" ], )"
                R"(     "mimeinfo.cache": [ "[MIME Cache]", )"
                R"(                         "foo/bar=foo.desktop" ] )"
                R"(   } )"
                R"( }, )"
                R"( "data_dir2": {} )"
            "}";
            auto const& value = json::parse(tree);
            create_directory_tree(tmp.path().c_str(), value.get_object());

            test::scoped_env e1 { "XDG_CONFIG_HOME", make_path(tmp.path(), "config_home") };
            test::scoped_env e2 { "XDG_DATA_HOME", make_path(tmp.path(), "data_home") };
            test::scoped_env e3 { "XDG_CONFIG_DIRS", make_path(tmp.path(), "config_dir1", "config_dir2") };
            test::scoped_env e4 { "XDG_DATA_DIRS", make_path(tmp.path(), "data_dir1", "data_dir2") };

            xdg::mime::database mime_db;
            apps::database db { mime_db };

            auto o_app = db.find_default_application("foo/bar");
            BOOST_TEST_REQUIRE(!!o_app);
            BOOST_TEST(o_app->name() == "FooHome");
        }

    BOOST_AUTO_TEST_SUITE_END() // find_default_application_

    BOOST_AUTO_TEST_SUITE(all_applications_)

        BOOST_AUTO_TEST_CASE(real_data_1_)
        {
            auto const& data_path = data_dir() / "real_data/data_dir";
            auto dummy = data_dir() / "dummy";
            test::scoped_env e1 { "XDG_CONFIG_HOME", dummy.c_str() };
            test::scoped_env e2 { "XDG_CONFIG_DIRS", dummy.c_str() };
            test::scoped_env e3 { "XDG_DATA_HOME", dummy.c_str() };
            test::scoped_env e4 { "XDG_DATA_DIRS", data_path.c_str() };

            xdg::mime::database mime_db;
            apps::database db { mime_db };
            test::ostream_captureer os { std::cerr };

            auto apps = db.all_applications();

            BOOST_TEST(apps.size() == 185);
        }

    BOOST_AUTO_TEST_SUITE_END() // all_applications_

    BOOST_AUTO_TEST_SUITE(associated_mime_types_)

        BOOST_AUTO_TEST_CASE(real_data_1_)
        {
            auto const& data_path = data_dir() / "real_data/data_dir";
            auto dummy = data_dir() / "dummy";
            test::scoped_env e1 { "XDG_CONFIG_HOME", dummy.c_str() };
            test::scoped_env e2 { "XDG_CONFIG_DIRS", dummy.c_str() };
            test::scoped_env e3 { "XDG_DATA_HOME", dummy.c_str() };
            test::scoped_env e4 { "XDG_DATA_DIRS", data_path.c_str() };

            xdg::mime::database mime_db;
            apps::database db { mime_db };
            test::ostream_captureer os { std::cerr };

            auto result = db.associated_mime_types();

            BOOST_TEST(result.size() == 338);

            result = db.associated_mime_types("x-scheme-handler/");

            BOOST_TEST(result.size() == 22);
        }

    BOOST_AUTO_TEST_SUITE_END() // associated_mime_types_

BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
