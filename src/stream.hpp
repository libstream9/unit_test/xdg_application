#ifndef STREAM9_DESKTOP_TEST_SRC_STREAM_HPP
#define STREAM9_DESKTOP_TEST_SRC_STREAM_HPP

#include <ostream>
#include <optional>

namespace std {

template<typename T>
std::ostream&
operator<<(std::ostream& os, std::optional<T> const& v)
{
    if (v) {
        os << *v;
    }
    else {
        os << "nullopt";
    }

    return os;
}

} // namespace std

#endif // STREAM9_DESKTOP_TEST_SRC_STREAM_HPP
